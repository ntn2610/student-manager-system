-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 15, 2019 at 06:10 AM
-- Server version: 10.3.15-MariaDB
-- PHP Version: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `asdasdasd`
--

-- --------------------------------------------------------

--
-- Table structure for table `diems`
--

CREATE TABLE `diems` (
  `id` int(10) NOT NULL,
  `diemcc` float DEFAULT NULL,
  `diemgk` float DEFAULT NULL,
  `diemck` float DEFAULT NULL,
  `diemthilai` float DEFAULT NULL,
  `monhoc_id` int(10) DEFAULT NULL,
  `sinhvien_id` int(10) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `diems`
--

INSERT INTO `diems` (`id`, `diemcc`, `diemgk`, `diemck`, `diemthilai`, `monhoc_id`, `sinhvien_id`, `created_at`, `updated_at`) VALUES
(128, 3, 2, 5, NULL, 12, 25, NULL, NULL),
(161, 1, 1, 1, NULL, 1, 21, NULL, NULL),
(162, 1, 1, 1, NULL, 2, 22, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `giangviens`
--

CREATE TABLE `giangviens` (
  `id` int(10) NOT NULL,
  `magv` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `hogv` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tengv` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ngaysinh` date NOT NULL,
  `gioitinh` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `giangviens`
--

INSERT INTO `giangviens` (`id`, `magv`, `hogv`, `tengv`, `ngaysinh`, `gioitinh`, `created_at`, `updated_at`) VALUES
(1, 'GV01', 'Trần', 'Thị Mỹ Hiền', '1992-02-02', 0, '2018-01-11 08:00:26', '2018-01-11 08:00:26'),
(15, 'GV02', 'Đỗ', 'Văn Tuấn', '2018-01-09', 1, '2018-01-11 08:03:35', '2018-01-11 08:03:35'),
(16, 'GV03', 'Bùi', 'Chí Thành', '2018-01-05', 1, '2018-01-11 08:04:39', '2018-01-11 08:04:39'),
(17, 'GV04', 'Lê', 'Thị Giang', '2018-01-02', 0, '2018-01-11 08:05:10', '2018-01-11 08:05:10'),
(18, 'GV05', 'Cao', 'Mạnh Hùng', '2018-01-26', 1, '2018-01-11 08:06:27', '2018-01-11 08:06:27'),
(19, 'GV06', 'Hà', 'Văn Muôn', '2018-01-08', 1, '2018-01-11 08:07:21', '2018-01-11 08:07:21'),
(20, 'GV07', 'Đào', 'Văn Tôn', '2018-01-03', 1, '2018-01-11 08:08:27', '2018-01-11 08:08:27'),
(21, 'GV08', 'Phan', 'Thanh Sơn', '2018-01-03', 1, '2018-01-11 08:10:22', '2018-01-11 08:10:22'),
(22, 'GV09', 'Nguyễn', 'Thanh Hải', '2018-01-03', 1, '2018-01-11 08:10:45', '2018-01-11 08:10:45'),
(23, 'GV10', 'Đinh', 'Văn Thế', '2018-01-03', 1, '2018-01-11 08:11:07', '2018-01-11 08:11:07'),
(27, 'GV11', 'Nguyễn', 'Văn Đoàn', '1998-03-03', 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `hockys`
--

CREATE TABLE `hockys` (
  `id` int(10) NOT NULL,
  `hocky` int(11) NOT NULL,
  `namhoc` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `hockys`
--

INSERT INTO `hockys` (`id`, `hocky`, `namhoc`) VALUES
(1, 1, '2017-2018'),
(2, 2, '2017-2018');

-- --------------------------------------------------------

--
-- Table structure for table `khoas`
--

CREATE TABLE `khoas` (
  `id` int(10) NOT NULL,
  `makhoa` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tenkhoa` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `khoas`
--

INSERT INTO `khoas` (`id`, `makhoa`, `tenkhoa`, `created_at`, `updated_at`) VALUES
(1, 'ĐC', 'Công nghệ thông tin', NULL, NULL),
(2, 'ĐV', 'Đại học viễn thông', NULL, NULL),
(3, 'doan', 'doan', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `lops`
--

CREATE TABLE `lops` (
  `id` int(10) NOT NULL,
  `malop` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tenlop` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `khoa_id` int(10) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `lops`
--

INSERT INTO `lops` (`id`, `malop`, `tenlop`, `khoa_id`, `created_at`, `updated_at`) VALUES
(1, 'ĐHCN3A', 'Đại học công nghệ 3A', 1, '2018-01-11 07:52:07', '2018-01-11 07:52:07'),
(2, 'ĐHCN3B', 'Đại học công nghệ 3B', 1, '2018-01-11 07:52:42', '2018-01-11 07:52:42'),
(3, 'ĐHCN3C', 'Đại học công nghệ 3C', 1, '2018-01-11 07:53:08', '2018-01-11 07:53:08'),
(4, 'ĐHVT13', 'Đại học viễn thông 13', 2, '2018-01-11 07:54:03', '2018-01-11 07:54:03');

-- --------------------------------------------------------

--
-- Table structure for table `monhocs`
--

CREATE TABLE `monhocs` (
  `id` int(10) NOT NULL,
  `mamon` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tenmon` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'tên môn học',
  `sotinchi` int(11) NOT NULL,
  `sotiet` int(11) NOT NULL,
  `hocky_id` int(10) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `monhocs`
--

INSERT INTO `monhocs` (`id`, `mamon`, `tenmon`, `sotinchi`, `sotiet`, `hocky_id`, `created_at`, `updated_at`) VALUES
(1, '4204', 'Cơ sở dữ liệu', 3, 75, 1, NULL, NULL),
(2, '4208', 'Lập trình Java', 4, 75, 1, NULL, NULL),
(3, '3306', 'Cấu kiện điện tử', 3, 65, 1, NULL, NULL),
(4, '2206', 'Giáo dục thể chất', 1, 75, 1, NULL, NULL),
(5, '4225', 'Nhập môn lập trình', 4, 70, 1, NULL, NULL),
(6, '2208', 'Toán rời rạc', 2, 65, 1, NULL, NULL),
(7, '3308', 'Công nghệ phần mềm', 4, 75, 1, NULL, NULL),
(8, '2216', 'Giải tích 1', 2, 65, 1, NULL, NULL),
(10, '4306', 'Kỹ năng mềm', 2, 45, 1, NULL, NULL),
(11, '4221', 'Lập trình web', 4, 75, 2, NULL, NULL),
(12, '1107', 'Bóng chuyền', 1, 45, 2, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `sinhviens`
--

CREATE TABLE `sinhviens` (
  `id` int(10) NOT NULL,
  `masv` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `hosv` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tensv` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `gioitinh` tinyint(1) NOT NULL,
  `ngaysinh` date NOT NULL,
  `quequan` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `lop_id` int(10) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sinhviens`
--

INSERT INTO `sinhviens` (`id`, `masv`, `hosv`, `tensv`, `gioitinh`, `ngaysinh`, `quequan`, `lop_id`, `created_at`, `updated_at`) VALUES
(21, '18ĐC001', 'Nguyễn', 'Văn Đoàn', 1, '1999-09-09', 'Vũng Sình', 1, NULL, NULL),
(22, '18ĐC009', 'Nguyễn', 'Duy Ngọc', 1, '1999-09-09', 'Bà Rịa Vũng Tàu', 1, NULL, NULL),
(25, '18ĐC007', 'Nguyễn', 'Duy Ngọc', 1, '1996-05-09', 'Bà Rịa Vũng Tàu', 2, NULL, NULL),
(28, '18ĐC000', 'Hoàng', 'Duy Quang', 1, '1994-09-07', 'Hải Phòng', 1, NULL, NULL),
(29, '18ĐC008', 'Trần', 'Duy Tuấn', 0, '1996-05-09', 'Hải Phòng', 2, NULL, NULL),
(31, '18ĐC010', 'Huỳnh', 'Diễm Anh', 0, '1996-05-09', 'Đà Nẵng', 2, NULL, NULL),
(32, '123', 'cuong', 'cuong', 1, '0000-00-00', 'cuong', 1, NULL, NULL),
(35, '1234', 'cuong', 'cuong', 1, '0000-00-00', 'cuong', 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `created_at`, `updated_at`) VALUES
(3, 'admin', 'admin@gmail.com', '7c4a8d09ca3762af61e59520943dc26494f8941b', NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `diems`
--
ALTER TABLE `diems`
  ADD PRIMARY KEY (`id`),
  ADD KEY `diems_sinhvien_id_foreign` (`sinhvien_id`),
  ADD KEY `diems_monhoc_id_foreign` (`monhoc_id`);

--
-- Indexes for table `giangviens`
--
ALTER TABLE `giangviens`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `giangviens_magv_unique` (`magv`);

--
-- Indexes for table `hockys`
--
ALTER TABLE `hockys`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `khoas`
--
ALTER TABLE `khoas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `lops`
--
ALTER TABLE `lops`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `lops_malop_unique` (`malop`),
  ADD KEY `lops_khoa_id_foreign` (`khoa_id`);

--
-- Indexes for table `monhocs`
--
ALTER TABLE `monhocs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `monhocs_mamon_unique` (`mamon`),
  ADD KEY `monhocs_hocky_id_foreign` (`hocky_id`);

--
-- Indexes for table `sinhviens`
--
ALTER TABLE `sinhviens`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `sinhviens_masv_unique` (`masv`),
  ADD KEY `sinhviens_lop_id_foreign` (`lop_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `diems`
--
ALTER TABLE `diems`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=163;

--
-- AUTO_INCREMENT for table `giangviens`
--
ALTER TABLE `giangviens`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `hockys`
--
ALTER TABLE `hockys`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `khoas`
--
ALTER TABLE `khoas`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `lops`
--
ALTER TABLE `lops`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `monhocs`
--
ALTER TABLE `monhocs`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `sinhviens`
--
ALTER TABLE `sinhviens`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `diems`
--
ALTER TABLE `diems`
  ADD CONSTRAINT `diems_monhoc_id_foreign` FOREIGN KEY (`monhoc_id`) REFERENCES `monhocs` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `diems_sinhvien_id_foreign` FOREIGN KEY (`sinhvien_id`) REFERENCES `sinhviens` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `lops`
--
ALTER TABLE `lops`
  ADD CONSTRAINT `lops_khoa_id_foreign` FOREIGN KEY (`khoa_id`) REFERENCES `khoas` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `monhocs`
--
ALTER TABLE `monhocs`
  ADD CONSTRAINT `monhocs_hocky_id_foreign` FOREIGN KEY (`hocky_id`) REFERENCES `hockys` (`id`);

--
-- Constraints for table `sinhviens`
--
ALTER TABLE `sinhviens`
  ADD CONSTRAINT `sinhviens_lop_id_foreign` FOREIGN KEY (`lop_id`) REFERENCES `lops` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
