<?php 
	//Kết nôi cơ sở dữ liệu.
	class Database{
		private $hostname  ='localhost';
		private $user_name ='root';
		private $pass 	   ='';
		private $dbname    ='quanlysinhvien_mvc';

		private $conn      = NULL;
		private $result    = NULL;

		public function connect(){
			$this->conn = new mysqli($this->hostname, $this->user_name, $this->pass, $this->dbname);
			if(!$this->conn){
				echo 'Kết nối thất bại';
				exit();
			}
			else {
				mysqli_set_charset($this->conn, 'utf8');
			}
			return $this->conn;
		}
// ------------------------------------------------------------------------------------------------------------------------------

		//Thực thi câu lệnh truy vấn.
		public function execute($sql){
			$this->result = $this->conn->query($sql); 
			return $this->result;
		}

// ------------------------------------------------------------------------------------------------------------------------------
		//Phương thức lấy dữ liệu.
		public function getData()
		{
			if($this->result){
				$data = mysqli_fetch_array($this->result);
			}
			else {
				$data = 0;
			}
			return $data;
		}

// ------------------------------------------------------------------------------------------------------------------------------		//Phương thức lấy toàn bộ dữ liệu.
		public function getAllData($table)
		{
			$sql = "SELECT * FROM $table";
			$this->execute($sql);	
			if($this->num_rows()==0){
				$data =0;

			}
			else{
				while($datas = $this->getData()){
					$data[]  = $datas;
				}
			}
			return $data;
		}

// ------------------------------------------------------------------------------------------------------------------------------

		//Phương thức lấy dữ liệu cần sửa theo ID.
		public function getDataID($table,$id)
		{
			$sql = "SELECT * FROM $table WHERE id ='$id'";
			$this->execute($sql);
			if($this->num_rows()!=0){
				$data = mysqli_fetch_array($this->result);
			}
			else {
				$data = 0;
			}
			return $data;
		}

// ------------------------------------------------------------------------------------------------------------------------------
		//Phương thức đếm số lượng bản ghi.
		public function num_rows(){
			if($this->result){
				$num = mysqli_num_rows($this->result);
			}
			else {
				$num = 0;
			}
			return $num;
		}
		
// ------------------------------------------------------------------------------------------------------------

		//Phương thức thêm dữ liệu.
		public function InsertData($malop, $hoten, $namsinh, $gioitinh, $quequan, $nganhhoc){
			$sql = "INSERT INTO sinhvien(id, malop, hoten, namsinh, gioitinh, quequan, nganhhoc)VALUES(null, '$malop', '$hoten', '$namsinh', '$gioitinh', '$quequan', '$nganhhoc')";
			return $this->execute($sql);
		}


// ------------------------------------------------------------------------------------------------------------
		//Phương thức sửa dữ liệu.
		public function UpdateData($id, $malop, $hoten, $namsinh, $gioitinh, $quequan, $nganhhoc){
			$sql = "UPDATE sinhvien SET malop ='$malop', hoten = '$hoten', namsinh = '$namsinh', gioitinh = '$gioitinh', quequan = '$quequan', nganhhoc = '$nganhhoc' WHERE id = '$id'";
			return $this->execute($sql);
		}

// ------------------------------------------------------------------------------------------------------------
		//Phương thức xóa dữ liệu.
		public function Delete($id,$table){
			$sql = "DELETE FROM $table WHERE id = '$id'";
			return $this->execute($sql);

		}


// ------------------------------------------------------------------------------------------------------------


		//Phương thức tìm kiếm dữ liệu theo từ khóa.
			public function SearchData($table, $tukhoa)
		{
			$sql = "SELECT * FROM $table WHERE hoten REGEXP '$tukhoa' ORDER BY id DESC ";
			$this->execute($sql);	
			if($this->num_rows()==0){
				$data =0;

			}
			else{
				while($datas = $this->getData()){
					$data[]  = $datas;
				}
			}
			return $data;
		}

 
 //-------------------------------------------------------------------------------------------------------------------------------
  											// --------------GIẢNG VIÊN----------------



  //Phương thức thêm dữ liệu.
		public function InsertData1($malop, $hoten, $namsinh, $gioitinh, $quequan){
			$sql = "INSERT INTO giangvien (id, malop, hoten, namsinh, gioitinh, quequan)VALUES(null, '$malop', '$hoten', '$namsinh', '$gioitinh', '$quequan')";
			return $this->execute($sql);
		}

// ------------------------------------------------------------------------------------------------------------------------------
		//Phương thức sửa dữ liệu.
		public function UpdateData1($id, $malop, $hoten, $namsinh, $gioitinh, $quequan){
			$sql = "UPDATE giangvien SET malop ='$malop', hoten = '$hoten', namsinh = '$namsinh', gioitinh = '$gioitinh', quequan = '$quequan' WHERE id = '$id'";
			return $this->execute($sql);
		}

// ------------------------------------------------------------------------------------------------------------------------------
		//Phương thức xóa dữ liệu.
		public function Delete1($id,$table){
			$sql = "DELETE FROM $table WHERE id = '$id'";
			return $this->execute($sql);

		}
// ------------------------------------------------------------------------------------------------------------------------------
		
		//Phương thức tìm kiếm dữ liệu theo từ khóa.
			public function SearchData1($table, $tukhoa)
		{
			$sql = "SELECT * FROM $table WHERE hoten REGEXP '$tukhoa' ORDER BY id DESC ";
			$this->execute($sql);	
			if($this->num_rows()==0){
				$data =0;

			}
			else{
				while($datas = $this->getData()){
					$data[]  = $datas;
				}
			}
			return $data;
		}






// -----------------------------------------------------------------------------------------------------------
											// --------------ĐIỂM SỐ----------------



  //Phương thức thêm dữ liệu.
		public function InsertData2($hoten, $malop, $diemlan1, $diemlan2){
			$sql = "INSERT INTO diem (id, hoten, malop, diemlan1, diemlan2)VALUES(null, '$hoten', '$malop', '$diemlan1', '$diemlan2')";
			return $this->execute($sql);
		}

// ------------------------------------------------------------------------------------------------------------------------------
		//Phương thức sửa dữ liệu.
		public function UpdateData2($id, $hoten, $malop, $diemlan1, $diemlan2){
			$sql = "UPDATE diem SET hoten ='$hoten', malop = '$malop', diemlan1 = '$diemlan1', diemlan2 = '$diemlan2', WHERE id = '$id'";
			return $this->execute($sql);
		}

// ------------------------------------------------------------------------------------------------------------------------------
		//Phương thức xóa dữ liệu.
		public function Delete2($id,$table){
			$sql = "DELETE FROM $table WHERE id = '$id'";
			return $this->execute($sql);

		}
// ------------------------------------------------------------------------------------------------------------------------------
		
		//Phương thức tìm kiếm dữ liệu theo từ khóa.
			public function SearchData2($table, $tukhoa)
		{
			$sql = "SELECT * FROM $table WHERE hoten REGEXP '$tukhoa' ORDER BY id DESC ";
			$this->execute($sql);	
			if($this->num_rows()==0){
				$data =0;

			}
			else{
				while($datas = $this->getData()){
					$data[]  = $datas;
				}
			}
			return $data;
		}
	}
 ?>
