<?php 
	

	if(isset($_GET['action'])){
		$action = $_GET['action'];
	}
	else {
		$action = '';	
	}


$thanhcong = array();


 	switch ($action){
 		case 'add':{
 			
 			if(isset($_POST['add_user'])){
 				$malop		= $_POST['malop'];
 				$hoten 		= $_POST['hoten'];
 				$namsinh 	= $_POST['namsinh'];
 				$gioitinh	= $_POST['gioitinh'];
 				$quequan 	= $_POST['quequan'];
 				$nganhhoc	= $_POST['nganhhoc'];

 				if($db->InsertData($malop, $hoten, $namsinh, $gioitinh, $quequan, $nganhhoc));
 				{
 					$thanhcong[] = 'add_success';
 				}
 					
 			}


 			require_once('View/sinhvien/add_user.php');
 			break;
 		}

			
// ------------------------------------------------------------------------------------------------------------------------------

		case 'edit':{
			if(isset($_GET['id'])){
				$id 		= $_GET['id'];
				$tblTable 	= 'sinhvien';
				$dataID 	= $db->getDataID($tblTable,$id);

				if(isset($_POST['update_user'])){

					//Lấy dữ liệu từ View.
					$malop 		= $_POST['malop'];
					$hoten 		= $_POST['hoten'];
					$namsinh 	= $_POST['namsinh'];
					$gioitinh 	= $_POST['gioitinh'];
					$quequan 	= $_POST['quequan'];
					$nganhhoc   = $_POST['nganhhoc'];

					//Truyền dữ liệu lấy từ View sang Model.
					if($db->UpdateData($id, $malop, $hoten, $namsinh, $gioitinh, $quequan, $nganhhoc)){
						header('location:index.php?controller=sinh-vien&action=list');
					}

				}
			}
			require_once('View/sinhvien/edit_user.php');
		}
			break;

// ------------------------------------------------------------------------------------------------------------------------------
		case 'delete':{
			if(isset($_GET['id'])){
				$id = $_GET['id'];
				$tblTable = 'sinhvien';

				if($db->Delete($id, $tblTable)){
					header('location:index.php?controller=sinh-vien&action=list');
				}
			}
			else {
				header('location:index.php?controller=sinh-vien&action=list');
			}
		}
			break;


// ------------------------------------------------------------------------------------------------------------------------------

		case 'list':{
			$tblTable = "sinhvien";
			$data = $db->getAllData($tblTable);
			require_once('View/sinhvien/list.php');
		}
			break; 

// ------------------------------------------------------------------------------------------------------------------------------

		case'tim-kiem':{
			if(isset($_GET['tukhoa'])){
				$tukhoa = $_GET['tukhoa'];
				$tblTable ='sinhvien';

				//Lấy dữ liệu từ Model (file : DBConfig.php)
				$data_search = $db->SearchData($tblTable, $tukhoa);
			}
			require_once('View/sinhvien/search_user.php');
		}
			break;
// ------------------------------------------------------------------------------------------------------------------------------
		default :{
			require_once('View/sinhvien/list.php');
		}
			break;
	}
 ?>