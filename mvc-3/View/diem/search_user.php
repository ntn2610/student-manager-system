<div class="timkiem">
	<form action ="" method="GET">
		<table>
			<tr>
				<input type="hidden" name="controller" value="diem">
				<td>
					<input type="text" name="tukhoa" placeholder="Nhập từ khóa">
				</td>
				<td>
					<input type="submit" value="Tìm kiếm">
				</td>
			</tr>
		</table>
		<input type="hidden" name="action" value="tim-kiem">
	</form>

</div>
<div class="danhsach">
	<h3>Danh sách điểm</h3>
	<table border="1px solid #black;">
		<thead>
			<tr>
				<th>STT</th>
				<th>Tên sinh viên</th>
				<th>Mã lớp</th>
				<th>Điểm lần 1</th>
				<th>Điểm lần 2</th>
				<th>Chức năng</th>
			</tr>
		</thead>	
		<tbody>
			<?php 
			$stt = 1;
				foreach($data_search as $value){
			 ?>
			<tr>
				<td><?php echo $stt; ?></td>
				<td><?php echo $value['hoten']; ?></td>
				<td><?php echo $value['malop']; ?></td>	
				<td><?php echo $value['diemlan1']; ?></td>	
				<td><?php echo $value['diemlan2']; ?></td>
				<td>
					<a onclick="return confirm('Bạn có chắc muốn sửa không ?')" href="index.php?controller=diem&action=edit&id=<?php echo $value['id']; ?>">Edit</a>

					<a onclick="return confirm('Bạn có chắc muốn xóa không ?')" href="index.php?controller=diem&action=delete&id=<?php echo $value['id']; ?>" title="Xóa">Delete</a>
				</td>
			</tr>
			<?php  
				$stt++;
			}
			?>
		</tbody>
</table>
</div>