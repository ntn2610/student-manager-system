<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Thêm mới điểm số</title>
</head>
<body>
		<div class="quanlysinhvien">
			<a href="index.php?controller=diem&action=list">Danh sách</a>
			<h3>Thêm mới điểm số</h3>
			<form action="" method="POST">
				<table>
					<tr>
						<td>Tên sinh viên :</td>
						<td><input type="text" name="hoten" placeholder="Tên sinh viên"></td>
					</tr>
					<tr>
						<td>Mã lớp :</td>
						<td><input type="text" name="malop" placeholder="Mã lớp"></td>
					</tr>
					<tr>
						<td>Điểm lần 1 :</td>
						<td><input type="text" name="diemlan1" placeholder="Điểm lần 1"></td>
					</tr>
					<tr>
						<td>Điểm lần 2 :</td>
						<td><input type="text" name="diemlan2" placeholder="Điểm lần 2"></td>
					</tr>
					<tr>
						<td>&nbsp;</td>
						<td><input type="submit" name="add_user" value="Thêm mới"></td>
					</tr>
				</table>
			</form>
			<?php 
			if(isset($thanhcong) && in_array('add_success', $thanhcong)){
				echo '<p style="color:black; text-align:center;">Thêm mới thành công</p>';
			}
		 ?>	
		</div>
</body>
</html>