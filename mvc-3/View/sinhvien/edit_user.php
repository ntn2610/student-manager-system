<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Sửa thông tin sinh viên</title>
	</style>
</head>
<body>
		<div class="quanlysinhvien">
			<a href="index.php?controller=sinh-vien&action=list">Danh sách</a>
			<h3>Cập nhật thông tin sinh viên</h3>
			<form action="" method="POST">
				<table>
					<tr>
						<td>Mã lớp :</td>
						<td><input type="text" name="malop" value="<?php echo $dataID['malop']; ?>" placeholder="Mã lớp"></td>
					</tr>
					<tr>
						<td>Tên sinh viên :</td>
						<td><input type="text" name="hoten" value="<?php echo $dataID['hoten']; ?>" placeholder="Tên sinh viên"></td>
					</tr>
					<tr>
						<td>Ngày sinh :</td>
						<td><input type="text" name="namsinh" value="<?php echo $dataID['namsinh']; ?>" placeholder="Năm sinh"></td>
					</tr>
					<tr>
						<td>Giới tính :</td>
						<td><input type="text" name="gioitinh" value="<?php echo $dataID['gioitinh']; ?>" placeholder="Giới tính"></td>
					</tr>
					<tr>
						<td>Quê quán :</td>
						<td><input type="text" name="quequan" value="<?php echo $dataID['quequan']; ?>" placeholder="Quê quán"></td>
					</tr>
					<tr>
						<td>Khoa Học :</td>
						<td><input type="text" name="nganhhoc" value="<?php echo $dataID['nganhhoc']; ?>" placeholder="Khoa Học"></td>
					</tr>
					<tr>
						<td>&nbsp;</td>
						<td><input type="submit" name="update_user" value="Cập nhật"></td>
					</tr>
				</table>
			</form>
		</div>
</body>
</html>