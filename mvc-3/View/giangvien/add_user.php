<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Thêm giang viên</title>
	</style>
</head>
<body>
		<div class="quanlysinhvien">
			<a href="index.php?controller=giang-vien&action=list">Danh sách</a>
			<h3>Thêm mới giảng viên</h3>
			<form action="" method="POST">
				<table>
					<tr>
						<td>Họ tên :</td>
						<td><input type="text" name="hoten" placeholder="Họ tên"></td>
					</tr>
					<tr>
						<td>Dạy lớp :</td>
						<td><input type="text" name="malop" placeholder="Dạy lớp"></td>
					</tr>
					<tr>
						<td>Ngày sinh :</td>
						<td><input type="text" name="namsinh" placeholder="Năm sinh"></td>
					</tr>
					<tr>
						<td>Giới tính :</td>
						<td><input type="text" name="gioitinh" placeholder="Giới tính"></td>
					</tr>
					<tr>
						<td>Quê quán :</td>
						<td><input type="text" name="quequan" placeholder="Quê quán"></td>
					</tr>
					<tr>
						<td>&nbsp;</td>
						<td><input type="submit" name="add_user" value="Thêm mới"></td>
					</tr>
				</table>
			</form>
			<?php 
			if(isset($thanhcong) && in_array('add_success', $thanhcong)){
				echo '<p style="color:black; text-align:center;">Thêm mới thành công</p>';
			}
		 ?>	
		</div>
</body>
</html>