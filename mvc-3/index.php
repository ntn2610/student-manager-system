<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="stylesheet" href="style.css">
	<link rel="stylesheet" type="text/css" href="bootstrap-4.3.1-dist/css/bootstrap.min.css">
	<script src="//cdnjs.cloudflare.com/ajax/libs/less.js/3.9.0/less.min.js" ></script>
	<title>Quản lý sinh viên-Giảng viên</title>
</head>
<body>
	<div class="menu">
		<nav class="navbar navbar-expand-lg ">
		  	<a class="navbar-brand" href="http://localhost:8080/mvc/index.php?controller=giang-vien&action=add">Giảng viên</a>
		   	<a class="navbar-brand" href="http://localhost:8080/mvc/index.php?controller=sinh-vien&action=add">Sinh viên</a>
		    <a class="navbar-brand" href="http://localhost:8080/mvc/index.php?controller=diem&action=add">Điểm</a>
		</nav>
	</div>



	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
  </body>
</body>
</html>

<?php 
	include "Model/DBConfig.php";
	$db = new Database;
	$db -> connect();

	if(isset($_GET['controller'])){
		$controller = $_GET['controller'];
	}
	else {
		$controller = '';	
	}

	switch ($controller){
		case 'sinh-vien':{
			require_once('Controller/sinhvien/index.php');
		}
	}
	switch ($controller){
		case 'giang-vien':{
			require_once('Controller/giangvien/index.php');
		}
	}


	switch ($controller){
		case 'diem':{
			require_once('Controller/diem/index.php');
		}
	}

 ?>