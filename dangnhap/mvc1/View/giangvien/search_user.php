<div class="timkiem">
	<form action ="" method="GET">
		<table>
			<tr>
				<input type="hidden" name="controller" value="giang-vien">
				<td>
					<input type="text" name="tukhoa" placeholder="Nhập từ khóa">
				</td>
				<td>
					<input type="submit" value="Tìm kiếm">
				</td>
			</tr>
		</table>
		<input type="hidden" name="action" value="tim-kiem">
	</form>
</div>
<div class="danhsach">
	<h3>Danh sách giảng viên</h3>
	<table border="1px solid #black;">
		<thead>
			<tr>
				<th>STT</th>
				<th>Mã giảng viên</th>
				<th>Họ giảng viên</th>
				<th>Tên giảng viên</th>
				<th>Ngày sinh</th>
				<th>Giới tính</th>
				<th>Chức năng</th>
			</tr>
		</thead>	
		<tbody>
			<?php 
			$stt = 1;
				foreach($data_search as $value){
			 ?>
			<tr>
				<td><?php echo $stt; ?></td>
				<td><?php echo $value['magv']; ?></td>
				<td><?php echo $value['hogv']; ?></td>
				<td><?php echo $value['tengv']; ?></td>
				<td><?php echo $value['ngaysinh']; ?></td>
				<td><?php echo $value['gioitinh']; ?></td>
				<td>
					<a onclick="return confirm('Bạn có chắc muốn sửa không ?')" href="index.php?controller=giang-vien&action=edit&id=<?php echo $value['id']; ?>">Edit</a>

					<a onclick="return confirm('Bạn có chắc muốn xóa không ?')" href="index.php?controller=giang-vien&action=delete&id=<?php echo $value['id']; ?>" title="Xóa">Delete</a>
				</td>
			</tr>
			<?php  
				$stt++;
			}
			?>
		</tbody>
</table>
</div>