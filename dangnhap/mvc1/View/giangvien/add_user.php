<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Thêm giang viên</title>
	</style>
</head>
<body>
		<div class="quanlysinhvien">
			<a href="index.php?controller=giang-vien&action=list">Danh sách</a>
			<h3>Thêm mới giảng viên</h3>
			<form action="" method="POST">
				<table>
					<tr>
						<td>Mã giảng viên :</td>
						<td><input type="text" name="magv" placeholder="Mã giảng viên"></td>
					</tr>
					<tr>
						<td>Họ giảng viên:</td>
						<td><input type="text" name="hogv" placeholder="Họ giảng viên"></td>
					</tr>
					<tr>
						<td>Tên giảng viên :</td>
						<td><input type="text" name="tengv" placeholder="Tên giảng viên"></td>
					</tr>
					<tr>
						<td>Ngày sinh :</td>
						<td><input type="text" name="ngaysinh" placeholder="Ngày sinh"></td>
					</tr>
					<tr>
						<td>Giới tính :</td>
						<td><input type="text" name="gioitinh" placeholder="Giới tính"></td>
					</tr>
					<tr>
						<td>&nbsp;</td>
						<td><input type="submit" name="add_giangvien" value="Thêm mới"></td>
					</tr>
				</table>
			</form>
			<?php 
			if(isset($thanhcong) && in_array('add_success', $thanhcong)){
				echo '<p style="color:black; text-align:center;">Thêm mới thành công</p>';
			}
		 ?>	
		</div>
</body>
</html>