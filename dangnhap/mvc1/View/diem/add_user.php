<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Thêm mới điểm</title>
</head>
<body>
		<div class="quanlysinhvien">
			<a href="index.php?controller=diem&action=list">Danh sách</a>
			<h3>Thêm mới điểm</h3>
			<form action="" method="POST">
				<table>
					<tr>
						<td>Điểm cuối cấp :</td>
						<td><input type="text" name="diemcc" placeholder="Điểm cuối cấp"></td>
					</tr>
					<tr>
						<td>Điểm giữa kì :</td>
						<td><input type="text" name="diemgk" placeholder="Điểm giữa kì"></td>
					</tr>
					<tr>
						<td>Điểm cuối kì :</td>
						<td><input type="text" name="diemck" placeholder="Điểm cuối kì"></td>
					</tr>
					<tr>
						<td>Mã môn học :</td>
						<td><input type="text" name="monhoc_id" placeholder="Mã môn học"></td>
					</tr>
					<tr>
						<td>Mã sinh viên :</td>
						<td><input type="text" name="sinhvien_id" placeholder="Mã sinh viên"></td>
					</tr>
						<td>&nbsp;</td>
						<td><input type="submit" name="add_diem" value="Thêm mới"></td>
					</tr>
				</table>
			</form>
			<?php 
			if(isset($thanhcong) && in_array('add_success', $thanhcong)){
				echo '<p style="color:black; text-align:center;">Thêm mới thành công</p>';
			}
		 ?>	
		</div>
</body>
</html>