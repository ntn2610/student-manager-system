<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Thêm sinh viên</title>
</head>
<body>
		<div class="quanlysinhvien">
			<a href="index.php?controller=sinh-vien&action=list">Danh sách</a>
			<h3>Thêm mới sinh viên</h3>
			<form action="" method="POST">
				<table>
					<tr>
						<td>Mã sinh viên :</td>
						<td><input type="text" name="masv" placeholder="Mã sinh viên"></td>
					</tr>
					<tr>
						<td>Họ sinh viên :</td>
						<td><input type="text" name="hosv" placeholder="Họ sinh viên"></td>
					</tr>
					<tr>
						<td>Tên sinh viên :</td>
						<td><input type="text" name="tensv" placeholder="Tên sinh viên"></td>
					</tr>
					<tr>
						<td>Giới tính :</td>
						<td><input type="text" name="gioitinh" placeholder="Giới tính"></td>
					</tr>
					<tr>
						<td>Ngày sinh :</td>
						<td><input type="text" name="ngaysinh" placeholder="Ngày sinh"></td>
					</tr>
					<tr>
						<td>Quê quán :</td>
						<td><input type="text" name="quequan" placeholder="Quê quán"></td>
					</tr>
					<tr>
						<td>Id lớp :</td>
						<td><input type="text" name="lop_id" placeholder="ID lớp"></td>
					</tr>
					<tr>
						<td>&nbsp;</td>
						<td><input type="submit" name="add_user" value="Thêm mới"></td>
					</tr>
				</table>
			</form>
			<?php 
			if(isset($thanhcong) && in_array('add_success', $thanhcong)){
				echo '<p style="color:black; text-align:center;">Thêm mới thành công</p>';
			}
		 ?>	
		</div>
</body>
</html>