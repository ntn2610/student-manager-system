<?php 
	//Kết nôi cơ sở dữ liệu.
	class Database{
		private $hostname  ='localhost';
		private $user_name ='root';
		private $pass 	   ='';
		private $dbname    ='asdasdasd';

		private $conn      = NULL;
		private $result    = NULL;

		public function connect(){
			$this->conn = new mysqli($this->hostname, $this->user_name, $this->pass, $this->dbname);
			if(!$this->conn){
				echo 'Kết nối thất bại';
				exit();
			}
			else {
				mysqli_set_charset($this->conn, 'utf8');
			}
			return $this->conn;
		}
// -----------------------------------------------------------------------------------------------------------------------

		//Thực thi câu lệnh truy vấn.
		public function execute($sql){
			$this->result = $this->conn->query($sql); 
			return $this->result;
		}

// ------------------------------------------------------------------------------------------------------------------------------
		//Phương thức lấy dữ liệu.
		public function getData()
		{
			if($this->result){
				$data = mysqli_fetch_array($this->result);
			}
			else {
				$data = 0;
			}
			return $data;
		}

// -----------------------------------------------------------------------------------------------------------------------		//Phương thức lấy toàn bộ dữ liệu.
		public function getAllData($table)
		{
			$sql = "SELECT * FROM $table";
			$this->execute($sql);
			if($this->num_rows()==0){
				$data = 0;	

			}
			else{
				while($datas = $this->getData()){
					$data[]  = $datas;
				}
			}
			return $data;
		}

// ------------------------------------------------------------------------------------------------------------------------------

		//Phương thức lấy dữ liệu cần sửa theo ID.
		public function getDataID($table,$id)
		{
			$sql = "SELECT * FROM $table WHERE id ='$id'";
			$this->execute($sql);
			if($this->num_rows()!=0){
				$data = mysqli_fetch_array($this->result);
			}
			else {
				$data = 0;
			}
			return $data;
		}

// ------------------------------------------------------------------------------------------------------------------------------
		//Phương thức đếm số lượng bản ghi.
		public function num_rows(){
			if($this->result){
				$num = mysqli_num_rows($this->result);
			}
			else {
				$num = 0;
			}
			return $num;
		}
		
// ------------------------------------------------------------------------------------------------------------

		//Phương thức thêm dữ liệu.
		public function InsertData($masv, $hosv, $tensv, $gioitinh, $ngaysinh, $quequan,$lop_id)
		{
			$sql = "INSERT INTO sinhviens (masv, hosv, tensv, gioitinh, ngaysinh, quequan, lop_id)VALUES('$masv', '$hosv', '$tensv', '$gioitinh', '$ngaysinh', '$quequan', '$lop_id')";
			return $this->execute($sql);
		}


// ------------------------------------------------------------------------------------------------------------
		//Phương thức sửa dữ liệu.
		public function UpdateData($id, $masv, $hosv, $tensv, $gioitinh, $ngaysinh, $quequan, $lop_id)
		{
			$sql = "UPDATE sinhviens SET masv ='$masv', hosv = '$hosv', tensv = '$tensv', gioitinh = '$gioitinh', ngaysinh = '$ngaysinh', quequan = '$quequan', lop_id = $lop_id WHERE id = '$id'";
			return $this->execute($sql);
		}

// ------------------------------------------------------------------------------------------------------------
		//Phương thức xóa dữ liệu.
		public function Delete($id, $table){
			$sql = "DELETE FROM $table WHERE id = '$id'";
			return $this->execute($sql);

		}


// ------------------------------------------------------------------------------------------------------------


		//Phương thức tìm kiếm dữ liệu theo từ khóa.
			public function SearchData($table, $tukhoa)
			{
			$sql = "SELECT * FROM $table WHERE tensv REGEXP '$tukhoa' ORDER BY id DESC ";

			$this->execute($sql);

			if($this->num_rows()==0){
				$data =0;

			}
			else{
				while($datas = $this->getData()){
					$data[]  = $datas;
				}
			}
			return $data;
		}

 
 //-------------------------------------------------------------------------------------------------------------------------------
  											// --------------GIẢNG VIÊN----------------



  //Phương thức thêm dữ liệu.
		public function InsertData1($magv, $hogv, $tengv, $ngaysinh, $gioitinh)
		{
			$sql = "INSERT INTO giangviens (magv, hogv, tengv, ngaysinh, gioitinh)VALUES('$magv', '$hogv', '$tengv', '$ngaysinh', '$gioitinh')";
			return $this->execute($sql);
		}

// ------------------------------------------------------------------------------------------------------------------------------
		//Phương thức sửa dữ liệu.
		public function UpdateData1($id, $magv, $hogv, $tengv, $ngaysinh, $gioitinh)
		{
			$sql = "UPDATE giangviens SET magv ='$magv', hogv = '$hogv', tengv = '$tengv', ngaysinh = '$ngaysinh', gioitinh = '$gioitinh' WHERE id = '$id'";
			return $this->execute($sql);
		}

// ------------------------------------------------------------------------------------------------------------------------------
		//Phương thức xóa dữ liệu.
		public function Delete1($id,$table){
			$sql = "DELETE FROM $table WHERE id = '$id'";
			return $this->execute($sql);

		}
// ------------------------------------------------------------------------------------------------------------------------------
		
		//Phương thức tìm kiếm dữ liệu theo từ khóa.
			public function SearchData1($table, $tukhoa)
		{
			$sql = "SELECT * FROM $table WHERE tengv REGEXP '$tukhoa' ORDER BY id DESC ";
			$this->execute($sql);	
			if($this->num_rows()==0){
				$data =0;

			}
			else{
				while($datas = $this->getData()){
					$data[]  = $datas;
				}
			}
			return $data;
		}






// -----------------------------------------------------------------------------------------------------------
											// --------------ĐIỂM SỐ----------------



  //Phương thức thêm dữ liệu.
		public function InsertData2($diemcc, $diemgk, $diemck, $monhoc_id, $sinhvien_id)
		{
			$sql = "INSERT INTO diems (diemcc, diemgk, diemck, monhoc_id, sinhvien_id)
			VALUES('$diemcc', '$diemgk', '$diemck', '$monhoc_id', '$sinhvien_id')";
		 	return $this->execute($sql);
		}

// -----------------------------------------------------------------------------------------------------------------------
		//Phương thức sửa dữ liệu.
		public function UpdateData2($id, $diemcc, $diemgk, $diemck, $monhoc_id, $sinhven_id)
		{
			$sql = "UPDATE diems SET diemcc ='$diemcc', diemgk = '$diemgk', diemck = '$diemck',monhoc_id = $monhoc_id, sinhven_id = $sinhven_id WHERE id = '$id'";
			return $this->execute($sql);
		}

// ------------------------------------------------------------------------------------------------------------------------------
		//Phương thức xóa dữ liệu.
		public function Delete2($id,$table){
			$sql = "DELETE FROM $table WHERE id = '$id'";
			return $this->execute($sql);

		}
// ------------------------------------------------------------------------------------------------------------------------------
		
		//Phương thức tìm kiếm dữ liệu theo từ khóa.
			public function SearchData2($table, $tukhoa)
		{
			$sql = "SELECT * FROM $table WHERE sinhvien_id REGEXP '$tukhoa' ORDER BY id DESC ";
			$this->execute($sql);	
			if($this->num_rows()==0){
				$data =0;

			}
			else{
				while($datas = $this->getData()){
					$data[]  = $datas;
				}
			}
			return $data;
		}
	}
 ?>
