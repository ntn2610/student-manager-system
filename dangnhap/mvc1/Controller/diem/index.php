<?php 
	

	if(isset($_GET['action'])){
		$action = $_GET['action'];
	}
	else {
		$action = '';	
	}


$thanhcong = array();

	$tblTable = 'diems';

 	switch ($action){
 		case 'add':{
 			if(isset($_POST['add_diem'])){
 				$diemcc			= $_POST['diemcc'];
 				$giuaKy 		= $_POST['diemgk'];
 				$cuoiKy 		= $_POST['diemck'];
 				$monhoc_id 		= $_POST['monhoc_id'];
 				$sinhvien_id	= $_POST['sinhvien_id'];
 				
 				if($db->InsertData2($diemcc, $giuaKy, $cuoiKy, $monhoc_id, $sinhvien_id))
 				{
 					$thanhcong[] = 'add_success';
 				}
 			}
 			require_once('View/diem/add_user.php');
 			break;
 		}

			
// -----------------------------------------------------------------------------------------------------------------------

		case 'edit':{
			if(isset($_GET['id'])){
				$id 			= $_GET['id'];
				$dataID 		= $db->getDataID($tblTable,$id);

				if(isset($_POST['update_diem'])){

					//Lấy dữ liệu từ View.
					$diemcc			= $_POST['diemcc'];
	 				$diemgk 		= $_POST['diemgk'];
	 				$diemck 		= $_POST['diemck'];
	 				$monhoc_id 		= $_POST['monhoc_id'];
	 				$sinhvien_id 	= $_POST['sinhvien_id'];

					//Truyền dữ liệu lấy từ View sang Model.
					if($db->UpdateData2($id, $diemcc, $diemgk, $diemck, $monhoc_id, $sinhvien_id))
					{
						header('location:index.php?controller=diem&action=list');
					}
				}
			}
			require_once('View/diem/edit_user.php');
		}
			break;

// ------------------------------------------------------------------------------------------------------------------------------
		case 'delete':{
			if(isset($_GET['id'])){
				$id = $_GET['id'];

				if($db->Delete2($id, $tblTable)){
					header('location:index.php?controller=diem&action=list');
				}
			}
			else {
					header('location:index.php?controller=diem&action=list');
			}
		}
			break;


// ------------------------------------------------------------------------------------------------------------------------------

		case 'list':{
			$data = $db->getAllData($tblTable);
			require_once('View/diem/list.php');
		}
			break; 

// ------------------------------------------------------------------------------------------------------------------------------

		case'tim-kiem':{
			if(isset($_GET['tukhoa'])){
				$tukhoa = $_GET['tukhoa'];

				//Lấy dữ liệu từ Model (file : DBConfig.php)
				$data_search = $db->SearchData2($tblTable, $tukhoa);
			}
			require_once('View/diem/search_user.php');
		}
			break;
// ------------------------------------------------------------------------------------------------------------------------------
		default :{
			require_once('View/diem/list.php');
		}
			break;
	}
 ?>