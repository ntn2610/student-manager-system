<?php 
	// require_once "logout.php"

	if(isset($_GET['action'])){
		$action = $_GET['action'];
	}
	else {
		$action = '';	
	}


$thanhcong = array();

	$tblTable = 'sinhviens';

 	switch ($action){
 		case 'add':{
 			if(isset($_POST['add_user'])){
 				$masv		= $_POST['masv'];
 				$hosv 		= $_POST['hosv'];
 				$tensv 		= $_POST['tensv'];
 				$gioitinh	= $_POST['gioitinh'];
 				$ngaysinh 	= $_POST['ngaysinh'];
 				$quequan	= $_POST['quequan'];
 				$lop_id		= $_POST['lop_id'];

 				if($db->InsertData($masv, $hosv, $tensv, $gioitinh, $ngaysinh, $quequan, $lop_id))
 				{
 					$thanhcong[] = 'add_success';
 				}
 			}
 			require_once('View/sinhvien/add_user.php');
 			break;
 		}

			
// -----------------------------------------------------------------------------------------------------------------------

		case 'edit':{
			if(isset($_GET['id'])){
				$id 			= $_GET['id'];
				$dataID 		= $db->getDataID($tblTable,$id);

				if(isset($_POST['update_user'])){

					//Lấy dữ liệu từ View.
					$masv		= $_POST['masv'];
	 				$hosv 		= $_POST['hosv'];
	 				$tensv 		= $_POST['tensv'];
	 				$gioitinh	= $_POST['gioitinh'];
	 				$ngaysinh 	= $_POST['ngaysinh'];
	 				$quequan	= $_POST['quequan'];
	 				$lop_id		= $_POST['lop_id'];
					//Truyền dữ liệu lấy từ View sang Model.
					if($db->UpdateData($id, $masv, $hosv, $tensv, $gioitinh, $ngaysinh, $quequan, $lop_id))
					{
						header('location:index.php?controller=sinh-vien&action=list');
					}
				}
			}
			require_once('View/sinhvien/edit_user.php');
		}
			break;

// ------------------------------------------------------------------------------------------------------------------------------
		case 'delete':{
			if(isset($_GET['id'])){
				$id = $_GET['id'];

				if($db->Delete($id, $tblTable)){
					header('location:index.php?controller=sinh-vien&action=list');
				}
			}
			else {
					header('location:index.php?controller=sinh-vien&action=list');
			}
		}
			break;


// ------------------------------------------------------------------------------------------------------------------------------

		case 'list':{
			$data = $db->getAllData($tblTable);
			require_once('View/sinhvien/list.php');
		}
			break; 

// ------------------------------------------------------------------------------------------------------------------------------

		case'tim-kiem':{
			if(isset($_GET['tukhoa'])){
				$tukhoa = $_GET['tukhoa'];

				//Lấy dữ liệu từ Model (file : DBConfig.php)
				$data_search = $db->SearchData($tblTable, $tukhoa);
			}
			require_once('View/sinhvien/search_user.php');
		}
			break;
// ------------------------------------------------------------------------------------------------------------------------------
		default :{
			require_once('View/sinhvien/list.php');
		}
			break;
	}
 ?>